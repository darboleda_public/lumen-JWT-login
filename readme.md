# Lumen PHP JWT login

1. composer install
2. configure your .env with .env.example, take care of the JWT_* fields in .env
3. php artisan migrate --seed
4. vendor\bin\phpunit tests