<?php

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\ResponseHelper;
use App\Jwt\JwtGenerator;
use App\User;

class AuthorizationTest extends TestCase
{   
    /**
     * User dataprovider
     * @return User
     */
    public function dataProviderUser()
    {
        return [
            [
                'user' => new User(
                    ['id' => '1', 'name' => 'TestUser', 'email' => 'test_user@gmail.com']
                )
            ]
        ];
    }

    /**
     * Test /users
     * @return void
     */
    public function testNotAllowed()
    {    
        $this->call('GET', '/users');          
        $this->assertEquals(
            ResponseHelper::getErrorResponseAsArray('NOT_ALLOWED')[ResponseHelper::KEY_HTTP_CODE],
            $this->response->getStatusCode()
        );
        $this->assertEquals(
            json_encode(ResponseHelper::getErrorResponseAsArray('NOT_ALLOWED')),
            $this->response->getContent()
        );
    }

    /**
     * Test /users
     * @return void
     */
    public function testTokenNotProvided()
    {    
        $this->get('/users', ['HTTP_Authorization' => 'Bearer ']);
        $this->assertEquals(
            ResponseHelper::getErrorResponseAsArray('TOKEN_NOT_PROVIDED')[ResponseHelper::KEY_HTTP_CODE],
            $this->response->getStatusCode()
        );
        $this->assertEquals(
            json_encode(ResponseHelper::getErrorResponseAsArray('TOKEN_NOT_PROVIDED')),
            $this->response->getContent()
        );
    }    

    /**
     * Test /users
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testExpiredToken(User $user)
    {
        $token = JwtGenerator::generate($user, 0);

        $this->get('/users', ['HTTP_Authorization' => 'Bearer ' . $token]);
        $this->assertEquals(
            ResponseHelper::getErrorResponseAsArray('TOKEN_EXPIRED')[ResponseHelper::KEY_HTTP_CODE],
            $this->response->getStatusCode()
        );
        $this->assertEquals(
            json_encode(ResponseHelper::getErrorResponseAsArray('TOKEN_EXPIRED')),
            $this->response->getContent()
        );
    }

    /**
     * Test /users
     * @return void
     */
    public function testInvalidToken()
    {    
        $token = 'pochoclo';
        $this->get('/users', ['HTTP_Authorization' => 'Bearer ' . $token]);
        $this->assertEquals(
            ResponseHelper::getErrorResponseAsArray('TOKEN_INVALID')[ResponseHelper::KEY_HTTP_CODE],
            $this->response->getStatusCode()
        );
        $this->assertEquals(
            json_encode(ResponseHelper::getErrorResponseAsArray('TOKEN_INVALID')),
            $this->response->getContent()
        );
    }

    /**
     * Test /users
     * @return void
     */
    public function testGetUsers()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];
        $this->get('/users', ['HTTP_Authorization' => 'Bearer ' . $token]);        
        $response = json_decode($this->response->getContent(),true);

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );                
        $this->assertArraySubset(
            ['email' => 'test_user@gmail.com'],
            $response[0]
        );
    }
}
