<?php

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\ResponseHelper;

class AuthTest extends TestCase
{
    /**
     * Test /auth/login
     * @return void
     */
    public function testLogin()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );        
        $response = json_decode($this->response->getContent(), true);
        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );                
        $this->assertArrayHasKey('token', $response);
    }

    /**
     * Test /auth/login
     * @return void
     */
    public function testInvalidLogin()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => 'badPassword'
            ]
        );        
        $this->assertEquals(
            ResponseHelper::getErrorResponseAsArray('USER_NOT_EXISTS')[ResponseHelper::KEY_HTTP_CODE],
            $this->response->getStatusCode()
        );


        $this->assertEquals(
            json_encode(ResponseHelper::getErrorResponseAsArray('USER_NOT_EXISTS')),
            $this->response->getContent()
        );
    }

    /**
     * Test /auth/login
     * @return void
     */
    public function testInvalidUser()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'badUser@gmail.com',
                'password' => 'badPassword'
            ]
        );        

        $this->assertEquals(
            ResponseHelper::getErrorResponseAsArray('USER_NOT_EXISTS')[ResponseHelper::KEY_HTTP_CODE],
            $this->response->getStatusCode()
        );
        $this->assertEquals(
            json_encode(ResponseHelper::getErrorResponseAsArray('USER_NOT_EXISTS')),
            $this->response->getContent()
        );
    }

    /**
     * Test /auth/register
     * @return void
     */
    public function testRegisterUserExists()
    {
        $this->post(
            '/auth/register',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345',
                'name' => 'TestUser'
            ]
        );        
        $this->assertEquals(
            ResponseHelper::getErrorResponseAsArray('USER_EXISTS')[ResponseHelper::KEY_HTTP_CODE],
            $this->response->getStatusCode()
        );
        $this->assertEquals(
            json_encode(ResponseHelper::getErrorResponseAsArray('USER_EXISTS')),
            $this->response->getContent()
        );
    }

    /**
     * Test /auth/register
     * @return void
     */
    public function testRegisterUser()
    {
        $this->post(
            '/auth/register',
            [
                'email' => 'test_user+1@gmail.com',
                'password' => '12345',
                'name' => 'TestUser 1'
            ]
        );        
        $response = json_decode($this->response->getContent(), true);
        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );                
        $this->assertArrayHasKey('token', $response);
    }
}
