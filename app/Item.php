<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Item extends Model
{	
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * Debug
     * @var boolean
     */
    protected $debug = false;

    const VALIDATION_RULES = [
        'title'     => 'required|max:250',
        'description'  => 'required|max:250',
        'image' => 'mimes:png,jpg,jpeg,gif'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'description', 'users_id'
    ];

    /**
     * Get files
     * @return App\ItemFile[]
     */
    public function files()
    {
        return $this->hasMany('App\ItemFile', 'items_id', 'id');
    }

    /**
     * Get user
     * @return App\User
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'users_id');
    }

    /**
     * Save user
     * 
     * @param  array $params
     * @return boolean 
     */
    public function saveItem($params)
    {	
    	DB::beginTransaction();
        $attached = $params['attached']??null;
        $params['created_at'] = Carbon::now()->toDateTimeString();
        unset($params['attached']);
        $id = DB::table($this->table)->insertGetId($params);
        if (!$id) {
            DB::rollBack();
            throw new \Exception('item not inserted');
            return false;
        }
        $item = new Item(
            array_merge($params, ['id' => $id])
        );
        
        if ($attached) {
            $files = [];
            $files[] = [
                'items_id' => $id,
                'file' => $attached,
                'created_at' => Carbon::now()->toDateTimeString()
            ];

            if (!DB::table('items_files')->insert($files)) {
                DB::rollBack();   
                throw new \Exception('File not saved in DB');
                return false;
            }           
        }    		
             
        DB::commit();
        return $item;
    }

    /**
     * Save user
     * 
     * @param  array $params
     * @return boolean 
     */
    public function updateItem($params)
    {   
        $id = $params['id'];
        unset($params['id']);
        $params['updated_at'] = Carbon::now()->toDateTimeString();

        $item = $this->getItem($id);
        if ($item->user->id != $params['users_id']) {
            throw new \Exception('Access denied');
        }
        DB::beginTransaction();
        $item = new Item(
            $params
        );        

        if ($params['attached']) {
            $files = [];
            $files[] = [
                'items_id' => $id,
                'file' => $params['attached'],
                'created_at' => Carbon::now()->toDateTimeString()
            ];

            //To save disk space, only one file per item            
            $itemFile = DB::table('items_files')->where('items_id', $id)->first();
            if ($itemFile) {
                @unlink(app()->basePath() .'/storage/app/public/'.$itemFile->file);
                if (!DB::table('items_files')->where('items_id', $id)->delete()) {
                    DB::rollBack();   
                    throw new \Exception('File not saved in DB');
                    return false;
                }
            }                

            if (!DB::table('items_files')->insert($files)) {
                DB::rollBack();   
                throw new \Exception('File not saved in DB');
                return false;
            }           
        }           

        unset($params['attached']);
        self::where('id', $id)->update($params);
             
        DB::commit();
        return $item;
    }

    /**
     * 
     * @param  integer $id
     * @return mixed
     */
    public function getItem($id)
    {   
        return self::find($id);
    }

    /**
     * 
     * @param  array $requestFields
     * @return mixed
     */
    public function getItems($requestFields = [])
    {   
        return $this->buildQuery($requestFields);
    }    

    /**
     * @param  array $requestFields
     * @return [type]
     */
    private function buildQuery($requestFields = [])
    {   
        $queryValues  = [];
        $and = '';
        $strQuery = '';
        if ($this->debug) {
            DB::enableQueryLog();
        }            
        if (isset($requestFields['user_id']) && $requestFields['user_id']!='') {            
            $strQuery = $and.' users_id = ? ';
            $queryValues[] = $requestFields['user_id'];
            $and = 'and';
        }
        if (isset($requestFields['text']) && $requestFields['text']!='') {            
            $strQuery .= $and.' (title like ? or description like ?)';
            $queryValues[] = '%'.$requestFields['text'].'%';
            $queryValues[] = '%'.$requestFields['text'].'%';
        }
        if ($strQuery=='') {
            $result =  self::paginate(Pagination::ITEMS_PER_PAGE);            
        } else {            
            $result = self::whereRaw($strQuery, $queryValues)->paginate(Pagination::ITEMS_PER_PAGE);
        }
        if ($this->debug) {
            var_dump(DB::getQueryLog());
        }
        return $result;
    }

    /**
     * Standard item format response
     * 
     * @param  Item $tmpitem [description]
     * @return Array
     */
    public static function showFormatedItem($tmpitem)
    {
        return [
            'id' => $tmpitem->id,
            'title' => $tmpitem->title,
            'description' => $tmpitem->description,
            'files' => isset($tmpitem->files)?$tmpitem->files:[],
            'user' => isset($tmpitem->user)?$tmpitem->user:[]
        ];
    }
}