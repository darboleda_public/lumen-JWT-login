<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Message extends Model
{	
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * Debug
     * @var boolean
     */
    protected $debug = false;

    const VALIDATION_RULES = [
        'message'     => 'required|max:255',
        'users_id_to' => 'required',
        'items_id' => 'required'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'message', 'users_id_from', 'users_id_to', 'items_id'
    ];

    /**
     * Get files
     * @return App\ItemFile[]
     */
    public function item()
    {
        return $this->hasOne('App\Item', 'id', 'items_id');
    }

    /**
     * Get user from
     * @return App\User
     */
    public function from()
    {
        return $this->hasOne('App\User', 'id', 'users_id_from');
    }

    /**
     * Get user to
     * @return App\User
     */
    public function to()
    {
        return $this->hasOne('App\User', 'id', 'users_id_to');
    }

    /**
     * Save message
     * @param  array $params
     * @return integer lastInsertedId
     */
    public function saveMessage($params)
    {   
        $params['created_at'] = Carbon::now()->toDateTimeString();
        return DB::table($this->table)->insertGetId($params);
    }

    /**
     * 
     * @param  array $requestFields
     * @return mixed
     */
    public function getMessages($requestFields = [])
    {   
        return $this->buildQuery($requestFields);
    }

    /**
     * @param  array $requestFields
     * @return [type]
     */
    private function buildQuery($requestFields = [])
    {   
        $queryValues  = [];
        $and = '';
        $strQuery = '';
        if ($this->debug) {
            DB::enableQueryLog();
        }            
        if (isset($requestFields['user_id']) && $requestFields['user_id']!='') {            
            $strQuery = $and.' (users_id_to = ? or users_id_from = ?)';
            $queryValues[] = $requestFields['user_id'];
            $queryValues[] = $requestFields['user_id'];
            $and = 'and';
        }
        if (isset($requestFields['items_id']) && $requestFields['items_id']!='') {            
            $strQuery .= $and.' items_id = ? ';
            $queryValues[] = $requestFields['items_id'];
            $and = 'and';
        }
        if (isset($requestFields['parent_id']) && $requestFields['parent_id']!='') {            
            $strQuery .= $and.' parent_id = ? ';
            $queryValues[] = $requestFields['parent_id'];
            $and = 'and';
        }
        if ($strQuery=='') {
            $result =  self::paginate(Pagination::ITEMS_PER_PAGE);            
        } else {            
            $result = self::whereRaw($strQuery, $queryValues)->paginate(Pagination::ITEMS_PER_PAGE);
        }
        if ($this->debug) {
            echo '<pre>';
            print_r($requestFields);
            echo '</pre>';

            echo '<pre>';
            print_r($queryValues);
            echo '</pre>';

            echo '<pre>';
            print_r(DB::getQueryLog()[1]['query']);
            echo '</pre>'; 
        }
        return $result;
    }

    public function getGroupedMessages($requestFields)
    {   
        if ($this->debug) {
            DB::enableQueryLog();
        }            

        $groupeMessages = DB::table($this->table)
                   ->select(
                        'users_id_from',
                        'users_id_to',
                        'items_id',
                        DB::raw('MAX(id) AS id'),
                        DB::raw('COUNT(1) AS total')
                    )                   
                    ->where('users_id_to', $requestFields['user_id'])
                    ->orWhere('users_id_from', $requestFields['user_id'])
                    ->groupBy('parent_id');

        $messages = DB::table($this->table)
                ->joinSub($groupeMessages, 'gm', function($join) {
                    $join->on('messages.id', '=', 'gm.id');
                })
                ->join('items', 'items.id', '=', 'messages.items_id')
                ->leftJoin('items_files', 'items_files.items_id', '=', 'items.id')
                ->join('users', 'users.id', '=', 'messages.users_id_from')
                ->select('gm.total',
                        'messages.id',
                        'messages.message',
                        'messages.created_at',
                        'messages.parent_id',
                        'items.id as item_id',
                        'items.title',
                        'items.description',
                        'items_files.file',
                        'users.id as from_id',
                        'users.name',
                        'users.email',
                        'gm.users_id_from as users_id_from',
                        'gm.users_id_to as users_id_to'
                )
                ->paginate(Pagination::ITEMS_PER_PAGE);
        if ($this->debug) {
            echo '<pre>';
            print_r($requestFields['user_id']);
            echo '</pre>';

            echo '<pre>';
            print_r(DB::getQueryLog()[1]['query']);
            echo '</pre>';            
        }
        return $messages;
    }
}