<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\User;
use App\Helpers\ResponseHelper;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

/**
 * 
 */
class Authenticate
{   
    const HEADER_AUTH = 'Bearer ';

    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $header = $request->headers->get('authorization');        
        if (stripos($header, self::HEADER_AUTH) === false) {
            return ResponseHelper::getErrorResponse('NOT_ALLOWED');
        }
        $token = str_replace(self::HEADER_AUTH, '', $header);        
        if(!$token) {
            return ResponseHelper::getErrorResponse('TOKEN_NOT_PROVIDED');
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return ResponseHelper::getErrorResponse('TOKEN_EXPIRED');
        } catch(Exception $e) {
            return ResponseHelper::getErrorResponse('TOKEN_INVALID');
        }
        $user = User::find($credentials->sub);
        // Now let's put the user in the request class so that you can grab it from there
        $request->auth = $user;
        return $next($request);
    }
}
