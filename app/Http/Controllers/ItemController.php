<?php
namespace App\Http\Controllers;

use Validator;
use App\Item;
use Carbon\Carbon;
use App\Helpers\ResponseHelper;
use Illuminate\Support\Facades\Hash;
use \Gumlet\ImageResize;

class ItemController extends Controller 
{
    /**
     * Add a item.
     * 
     * @param  \App\Item   $item
     * @return mixed
     */
    public function addItem(Item $item) 
    {   
        if (!$this->request->auth) {
            return ResponseHelper::getErrorResponse('USER_DISABLED');
        }
        
        $keepOnly = Item::VALIDATION_RULES;
        $this->validate($this->request, $keepOnly);
        unset($keepOnly['image']);
        $postFields  = $this->processItemRequest($keepOnly);        
        $createdItem = $item->saveItem($postFields);
        
        return ResponseHelper::getResponse([
                'file_data' => $_FILES['image']??'',
                'item_data' => $createdItem
            ],
            __METHOD__
        );
    }    
    
    /**
     * Get the list of all items
     * 
     * @param  \App\Item   $item
     * @return mixed
     */
    public function getItems(Item $item) 
    {   
        $requestFields = $this->request->all();
        return $this->formatItemsResponse(
            $item->getItems($requestFields),
            __METHOD__
        );
    }

    /**
     * Get the list of all items
     * 
     * @param  \App\Item   $item
     * @return mixed
     */
    public function getMyItems(Item $item) 
    {           
        if (!$this->request->auth) {
            return ResponseHelper::getErrorResponse('USER_DISABLED');
        }

        $requestFields = $this->request->all();
        
        $requestFields['user_id'] = $this->request->auth->id;
        return $this->formatItemsResponse(
            $item->getItems($requestFields),
            __METHOD__
        );        
    }

    /**
     * Update an item
     * 
     * @param  \App\Item   $item
     * @return mixed
     */
    public function updateItem(Item $item) 
    {   
        if (!$this->request->auth) {
            return ResponseHelper::getErrorResponse('USER_DISABLED');
        }

        $keepOnly = Item::VALIDATION_RULES;
        $keepOnly['id'] = 'required';
        $this->validate($this->request, $keepOnly); 
        unset($keepOnly['image']);
        $postFields  = $this->processItemRequest($keepOnly);        
        $updatedItem = $item->updateItem($postFields);

        return ResponseHelper::getResponse([
                'file_data' => $_FILES['image']??'',
                'item_data' => $updatedItem
            ],
            __METHOD__
        );
    }

    /**
     * Standard response for items
     * 
     * @param  \App\Item[] $items
     * @param  string $method
     * @return PSR Response
     */
    private function formatItemsResponse($items, $method)
    {   
        $resItems = [];

        foreach ($items as $tmpitem) {
            $resItems[] =  Item::showFormatedItem($tmpitem);
        }
        
        return ResponseHelper::getResponse([
                'items' => $resItems,
            ],
            $method
        )->withHeaders([
            'Cache-Control' => 'public, max-age=60'
        ]);
    }

    /**
     * Upload image
     * 
     * @param  \App\Item   $item
     * @return mixed
     */
    public function upload(Item $item) 
    {   
        if (!$this->request->auth) {
            return ResponseHelper::getErrorResponse('USER_DISABLED');
        }        
        return ResponseHelper::getResponse(
            $this->processUpload(),
            __METHOD__
        );
    }

    /**
     * Process the request
     * @param array $keepOnly
     * @return array $postFields
     */
    private function processUpload()
    {
        $response = [];
        $postFields = $this->request->all();
        $response['file_data'] = $_FILES['image']??'';
        $imagePath = app()->basePath() .'/storage/app/public/';
        if ($this->request->file('image')) {
            try{
                $response['attached'] = $this->request->file('image')->store(
                    'items/'.Carbon::now()->format('Y/m/d/H'), 'public'
                );

                //save disk space
                $image = new ImageResize($imagePath.$response['attached']);
                $image->resizeToLongSide(400);
                $image->save($imagePath.$response['attached']);

            } catch(\Exception $e) {
                die($e->getMessage().__METHOD__.__LINE__);
            }                
        }        
        return $response;
    }

    /**
     * Process the request, keep only the valid params
     * @param array $keepOnly
     * @return array $postFields
     */
    protected function processItemRequest($keepOnly = [])
    {
        $response = $this->processUpload();        
        $postFields = $this->processRequest($keepOnly);
        $postFields['attached'] = isset($response['attached'])?$response['attached']:'';
        $postFields['users_id'] = $this->request->auth->id;
        return $postFields;
    }
}
