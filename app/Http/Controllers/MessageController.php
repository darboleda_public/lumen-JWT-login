<?php
namespace App\Http\Controllers;

use Validator;
use App\Message;
use App\Item;
use App\User;
use App\Helpers\ResponseHelper;
use Carbon\Carbon;

class MessageController extends Controller 
{
    /**
     * Add a meesage.
     * 
     * @param  \App\Message   $message
     * @return mixed
     */
    public function addMessage(Message $message) 
    {   
        if (!$this->request->auth) {
            return ResponseHelper::getErrorResponse('USER_DISABLED');
        }
        
        $keepOnly = Message::VALIDATION_RULES;
        $this->validate($this->request, $keepOnly);
        $postFields = $this->processRequest(array_merge($keepOnly,['parent_id'=>'']));        
        $postFields['users_id_from'] = $this->request->auth->id;
        if (!isset($postFields['parent_id'])) {
            $postFields['parent_id'] = $postFields['users_id_from'].'_'.$postFields['items_id'];
        }
        if ($postFields['users_id_to']==$postFields['users_id_from']) {
            $item = Item::find($postFields['items_id']);
            $postFields['users_id_to'] = $item->user->id;
        }
        $createdMessage = $message->saveMessage($postFields);
        
        return ResponseHelper::getResponse([
                'id' => $createdMessage
            ],
            __METHOD__
        );
    }

    /**
     * Get the list of all items
     * 
     * @param  \App\Message   $message
     * @return mixed
     */
    public function getMyMessages(Message $message) 
    {           
        if (!$this->request->auth) {
            return ResponseHelper::getErrorResponse('USER_DISABLED');
        }
        $requestFields = $this->processRequest(
            ['items_id'=>'optional', 'parent_id' => 'optional']
        );        
        $requestFields['user_id'] = $this->request->auth->id;
        return $this->formatMessagesResponse(
            $message->getMessages($requestFields),
            __METHOD__
        );        
    }

    /**
     * Get the list of all items
     * 
     * @param  \App\Message   $message
     * @return mixed
     */
    public function getMessagesResume(Message $message) 
    {           
        if (!$this->request->auth) {
            return ResponseHelper::getErrorResponse('USER_DISABLED');
        }
        $requestFields = $this->processRequest(
            ['items_id'=>'optional', 'parent_id' => 'optional']
        );        
        $requestFields['user_id'] = $this->request->auth->id;
        return $this->formatMessagesResume(
            $message->getGroupedMessages($requestFields),
            __METHOD__
        );        
    }

    /**
     * Standard response for items
     * 
     * @param  \App\Item[] $items
     * @param  string $method
     * @return PSR Response
     */
    private function formatMessagesResponse($messages, $method)
    {   
        $resMessages = [];

        foreach ($messages as $tmpMessage) {            
            $resMessages[] = [
                'id' => $tmpMessage->id,
                'message' => $tmpMessage->message,
                'item' => Item::showFormatedItem($tmpMessage->item),
                'from' => isset($tmpMessage->from)?$tmpMessage->from:[],
                'created' => $tmpMessage->created_at->format('d/m/Y H:i'),
                'created_raw' => $tmpMessage->created_at,
                'is_owner' => $tmpMessage->from->id == $this->request->auth->id,
                'parent_id' => $tmpMessage->parent_id
            ];            
        }

        return ResponseHelper::getResponse([
                'messages' => $resMessages,
            ],
            $method
        );
    }

     /**
     * Standard response for items
     * 
     * @param  \App\Item[] $items
     * @param  string $method
     * @return PSR Response
     */
    private function formatMessagesResume($messages, $method)
    {   
        $resMessages = [];

        foreach ($messages as $tmpMessage) {            
            $from_id = $tmpMessage->from_id;
            $from = $tmpMessage->email;

            if ($from_id == $this->request->auth->id){
                $from_id = $tmpMessage->users_id_to;
                $user = User::find($from_id);
                $from = $user->email;
            }

            if ($from_id == $this->request->auth->id){
                $from_id = $tmpMessage->users_id_from;
                $user = User::find($from_id);
                $from = $user->email;
            }

            $resMessages[] = [
                'id' => $tmpMessage->id,
                'message' => $tmpMessage->message,
                'item_id' => $tmpMessage->item_id,
                'item_title' => $tmpMessage->title,
                'item_file' => $tmpMessage->file,
                'item_description' => $tmpMessage->description,
                'from' => $from,
                'from_id' => $from_id,
                'created' => Carbon::parse($tmpMessage->created_at)->format('d/m/Y H:i'),
                'created_raw' => $tmpMessage->created_at,
                'parent_id' => $tmpMessage->parent_id
            ];            
        }

        return ResponseHelper::getResponse([
                'messages' => $resMessages,
            ],
            $method
        );
    }
}
