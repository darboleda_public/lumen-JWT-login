<?php

namespace App\Jwt;

use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtGenerator
{       
    /**
     * Create a new token.
     * 
     * @param  \App\User   $user
     * @param  integer $expiration in seconds
     * @return string
     */
    public static function generate(User $user, $expiration = null) {        
        $payload = [
            'iss' => env('JWT_TOKEN_ISS', 'lumen-jwt'), // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + ($expiration??env('JWT_TOKEN_EXPIRATION', 3600))
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }
}