<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{   
    private $table = 'messages';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists($this->table);
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id_from')->unsigned();
            $table->integer('users_id_to')->unsigned();
            $table->integer('items_id')->unsigned();
            $table->string('message');
            $table->string('parent_id');
            $table->timestamps();
        });

        Schema::table($this->table, function(Blueprint $table) {
           $table->foreign('users_id_from')
              ->references('id')->on('users')
              ->onDelete('cascade');

            $table->foreign('users_id_to')
              ->references('id')->on('users')
              ->onDelete('cascade');

            $table->foreign('items_id')
              ->references('id')->on('items')
              ->onDelete('cascade');

            $table->index('message');
            $table->index('parent_id');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
